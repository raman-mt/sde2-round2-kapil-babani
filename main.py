from datetime import date, timedelta
import pandas as pd
from init import s3, conn, AWS_BUCKET_PATH, AWS_S3_KEY_PATH
import botocore
import os


def report_generation():
    """

    :return:
    """
    try:
        cursor = conn.cursor()
        # -----------------------------get user id who are active----------------------------------
        query_1 = ''' select user_id 
                      from mindtickle_users 
                      where active_status = "active"'''
        # ------------------------------------------------------------------------------------------
        active_users = cursor.execute(query_1).fetchall()
        active_users = [active_user[0] for active_user in active_users]
        # create date filter with date module
        range_end = date.today().isoformat()
        range_start = (date.today() - timedelta(days=30)).isoformat()
        # ----------------using active id and date filter get lessons Data ------------------------
        query_2 = '''select count(user_id) as daily_session_count , user_id, completion_date
                     from lesson_completion
                     where completion_date >= "{RANGE_START}" and 
                           completion_date <= "{RANGE_END}" and 
                           user_id in {ACTIVE_USERS}
                     group by user_id, completion_date''' \
            .format(RANGE_START=range_start, RANGE_END=range_end, ACTIVE_USERS=tuple(active_users))
        # -----------------------------------------------------------------------------------------
        report_result = cursor.execute(query_2).fetchall()
        names = list(map(lambda x: x[0], cursor.description))
        report_df = pd.DataFrame(report_result, columns=names)
        file_name = '{DATE}_report.csv'.format(DATE=range_end)
        report_df.to_csv(file_name)
        s3.BucDEs   ket(AWS_BUCKET_PATH).upload_file(file_name, AWS_S3_KEY_PATH)
        # ----------------- delete file so it don't create garbage Data ------------------------
        os.remove(file_name)
        print(report_result)
        print("report is successfully generated and saved")
    except botocore.exceptions.ClientError as error:
        # Put your error handling logic here
        raise error
    except botocore.exceptions.ParamValidationError as error:
        raise ValueError('The parameters you provided are incorrect: {}'.format(error))
    except Exception as e:
        print(e)
    finally:
        conn.close()


if __name__ == '__main__':
    report_generation()
