import sqlite3
import boto3
from base64 import b64decode
import os

LOCAL_DB_NAME = os.environ.get('LOCAL_DB', 'mindtickle.db')
AWS_BUCKET_PATH = ''
cursor = None
AWS_S3_KEY_PATH = ''
DB_HOST = os.environ.get("DB_HOST")
DB_USER = os.environ.get("DB_USER")
DB_PASS_ENCRYPTED = os.environ.get("DB_PASS")
if DB_PASS_ENCRYPTED:
    cipherTextBlob = b64decode(DB_PASS_ENCRYPTED)
    DB_PASS_DECRYPTED = boto3.client('kms').decrypt(CiphertextBlob=cipherTextBlob)['Plaintext']

try:
    conn = sqlite3.connect(LOCAL_DB_NAME)

except sqlite3.Error as error:
    print('Error occurred - ', error)

s3 = boto3.resource('s3')

