import pandas as pd
# import pyodbc
import sqlite3

LOCAL_DB_NAME = 'mindtickle.db'


def run_sql_script(file_path):
    try:
        conn = sqlite3.connect(LOCAL_DB_NAME)
        cursor = conn.cursor()
        sql_file = open(file_path)
        sql_as_string = sql_file.read()
        cursor.executescript(sql_as_string)
        conn.commit()
    except sqlite3.Error as error:
        print('Error occurred - ', error)
    finally:
        if "conn" in locals() and conn:
            conn.close()
            print('SQLite Connection closed')


def create_table_in_db(table_name, csv_path, create_query):
    try:
        data = pd.read_csv(csv_path)
        df = pd.DataFrame(data)
        columns = df.columns
        conn = sqlite3.connect(LOCAL_DB_NAME)
        cursor = conn.cursor()
        cursor.execute(create_query)
        insert_statement = '''
                        INSERT INTO {TABLE_NAME} {COL_NAME}
                        VALUES (VALUES)
                        '''.format(TABLE_NAME=table_name, COL_NAME=columns)
        for row in df.itertuples():
            cursor.execute(insert_statement,
                           row.user_id, row.user_name, row.active_status
                           )
        conn.commit()
    except sqlite3.Error as error:
        print('Error occurred - ', error)
    finally:
        if "conn" in locals() and conn:
            conn.close()
            print('SQLite Connection closed')


run_sql_script('setup/init.mysql.sql')
run_sql_script('setup/init.pg.sql')
