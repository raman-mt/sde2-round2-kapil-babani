import pandas as pd
from pathlib import Path
import os
active_user = [(1, 'User1', 'active'),
    (2, 'User2', 'inactive'),
    (3, 'User3', 'active'),
    (4, 'User4', 'active')]

cols = ["user_id", "user_name", "active_status"]

df = pd.DataFrame(active_user, columns=cols)
# get current directory
parent_path = Path(os.getcwd()).parent.absolute()
file_path = os.path.join(parent_path, 'active_user.csv')
df.to_csv(file_path, index=False)
